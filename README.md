Lesstof over het modelleren met behulp van Coach 7. Geschreven voor VWO5.

In deze lessen bekijken we zowel grafische modellen als tekstmodellen. Met deze modellen zullen we mechanicaproblemen bekijken en zien dat we hiermee makkelijk de formules op kunnen lossen en snel verschillende situaties door kunnen rekenen. 