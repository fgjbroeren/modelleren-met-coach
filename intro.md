# Inleiding

Deze pagina's zijn bedoeld als aanvulling op hoofdstuk 18 van de methode [Het Geheim van de Dingen](http://hetgeheimvandedingen.nl/wp-content/uploads/2020/08/Modelleren-V-2019-pix.pdf). Hiermee gaan we iets dieper in op de relatie tussen formules en modellen en bespreken we ook grafisch modelleren.

## PDF

Een PDF van deze pagina's is [hier beschikbaar](https://gitlab.com/fgjbroeren/modelleren-met-coach/-/jobs/artifacts/main/raw/book.pdf?job=latex)
